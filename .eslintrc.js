require('@rushstack/eslint-patch/modern-module-resolution');

module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        sourceType: 'module',
        project: './tsconfig.json',
        ecmaVersion: 2019
    },
    plugins: ['react-hooks'],
    extends: [
        'plugin:react/recommended', // Uses the recommended rules from @eslint-plugin-react
        'plugin:react-hooks/recommended',
        'plugin:@typescript-eslint/recommended', // Uses the recommended rules from @typescript-eslint/eslint-plugin
        'plugin:prettier/recommended' // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    ],
    rules: {
        curly: ['warn', 'all'],
        eqeqeq: ['error', 'always'],
        '@typescript-eslint/no-unused-vars': [
            'warn',
            {
                vars: 'all',
                args: 'after-used',
                ignoreRestSiblings: true,
                destructuredArrayIgnorePattern: '^_'
            }
        ],
        '@typescript-eslint/no-misused-promises': ['error', {}],
        '@typescript-eslint/no-floating-promises': ['error', {}],
        '@typescript-eslint/strict-boolean-expressions': ['error', {}],
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'warn',
        'react/prop-types': 'off',
        'no-var': 'off'
    },
    ignorePatterns: [
        'types.d.ts',
        '/src/jira.swagger.types.ts',
        '/src/confluence.swagger.types.ts',
        'babel.config.js',
        'jest.config.ts',
        '.eslintrc.js',
        'dist',
        'lib'
    ]
};
