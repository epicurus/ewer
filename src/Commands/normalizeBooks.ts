import fs from 'fs';
import { isNil } from 'lodash';

import { readChapters } from '../common';
const defaultChapterNamesIndexDigits = 3;

const normalizeIndices = (
    directory: string,
    chapterNames: string[],
    indexDigits: number = defaultChapterNamesIndexDigits
): void => {
    chapterNames
        .sort((a, b) => (parseFloat(a) < parseFloat(b) ? -1 : 1))
        .forEach((chapterName, i) => {
            const match = chapterName.match(/^[\d.]+(.*)$/);
            const pureFileName = !isNil(match) ? match[1] : '';
            const indexString = `000${i}`.slice(-indexDigits);
            const fileName = `${indexString}${pureFileName}`;

            if (fileName !== chapterName) {
                fs.renameSync(
                    `${directory}/${chapterName}`,
                    `${directory}/${fileName}`
                );
            }
        });
};

const normalizeBooks = async (directory: string): Promise<void> => {
    const chapters = await readChapters(directory);

    normalizeIndices(directory, chapters, chapters.length.toString().length);
};

export default normalizeBooks;
