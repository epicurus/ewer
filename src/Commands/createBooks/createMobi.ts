import chalk from 'chalk';

import { execSync } from 'child_process';
import { PROJECT_ROOT, PUBLIC_DIR } from '../../common';

interface KindleError {
    stdout: string;
    message: string;
}

const createMobi = (bookName: string): string => {
    const epubPath = `${PROJECT_ROOT}/${PUBLIC_DIR}/${bookName}.epub`;

    const cmd = `kindlepreviewer ${epubPath} -convert`;

    console.log('Converting epub to kindle format.');

    try {
        execSync(cmd);
    } catch (err) {
        const error = err as KindleError;
        const stdout = `${error.stdout}`;
        if (!stdout.includes('Mobi file built')) {
            console.error(chalk.red(error.message));
            console.error(chalk.redBright(stdout));
            process.exit(1);
        }
    }

    return epubPath.replace('.epub', '.mobi');
};

export default createMobi;
