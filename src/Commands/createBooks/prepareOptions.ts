import { BookProductionOptions } from '../../types';
import { getWidthAndHeightOfCustomSize } from '../../common';

const setCustomSizeFromCm = (config: BookProductionOptions): void => {
    const customSizeCm = config.pdfOutputOptions.customSizeCm;

    if (customSizeCm !== undefined) {
        const { width, height } = getWidthAndHeightOfCustomSize(
            customSizeCm,
            1 / 2.54
        );

        if (width > 0 && height > 0) {
            config.pdfOutputOptions.customSize = `${width}x${height}`;
            delete config.pdfOutputOptions.customSizeCm;
        }
    }
};
const prepareOptions = (config: BookProductionOptions): void => {
    setCustomSizeFromCm(config);
};

export default prepareOptions;
