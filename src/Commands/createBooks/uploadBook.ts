import { GoogleCloudStorageFileIo } from '../../Components/RemoteFileIo';

const FileIo = new GoogleCloudStorageFileIo({ bucketName: 'ewer' });

const uploadBook = async (sourcePath: string): Promise<string> => {
    const result = await FileIo.upload(sourcePath, { public: true });

    if (result === null) {
        throw new Error(`Error uploading book ${sourcePath}`);
    }

    const { publicUrl } = result;
    console.log(`Uploaded book to '${publicUrl}'`);

    return publicUrl;
};

export const getPublicPath = (): string => {
    return FileIo.publicPath();
};

export default uploadBook;
