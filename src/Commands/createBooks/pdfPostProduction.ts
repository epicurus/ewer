import { PDFDocument } from 'pdf-lib';
import fs from 'fs';
import { PdfOutputOptions } from '../../types';
import { getWidthAndHeightOfCustomSize } from '../../common';

const pdfPostProduction = async (
    filePath: string,
    pdfOutputOptions: PdfOutputOptions
): Promise<void> => {
    const { customSize } = pdfOutputOptions;

    if (customSize !== undefined) {
        const { width, height } = getWidthAndHeightOfCustomSize(customSize);
        const targetWidth = width * 72;
        const targetHeight = height * 72;

        const existingPdfBytes = fs.readFileSync(filePath);
        // Load a PDFDocument from the existing PDF bytes
        const pdfDoc = await PDFDocument.load(existingPdfBytes);
        // Get the first page of the document
        const pages = pdfDoc.getPages();
        pages.forEach((page, i: number) => {
            // Get the width and height of the first page
            const { width: currentWidth, height: currentHeight } =
                page.getSize();

            if (
                targetWidth !== currentWidth ||
                targetHeight !== currentHeight
            ) {
                page.setSize(targetWidth, targetHeight);
                console.info(
                    `Changing width and height of pdf page ${i} to ${targetWidth} / ${targetHeight}`
                );
            }
        });

        const pdfBytes = await pdfDoc.save();
        fs.writeFileSync(filePath, pdfBytes);
    }
};

export default pdfPostProduction;
