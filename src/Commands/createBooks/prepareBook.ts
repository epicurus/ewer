import fs from 'fs';
import { isNil } from 'lodash';
import YAML from 'yaml';
import chalk from 'chalk';
import {
    readChapters,
    replaceVariables,
    TEMP_DIR,
    PROJECT_ROOT,
    MEDIA_DIR
} from '../../common';

import {
    BookProductionOptions,
    Metadata,
    MarkdownVariables,
    EbookType,
    AdditionalParameter
} from '../../types';

interface IFileData {
    len: number;
    fileName: string;
    fileData: string;
}

type ISourceFileNames = Record<string, string>;

const prepareFile = async (
    chapter: string,
    directory: string,
    metadata: Metadata,
    bookType: EbookType
): Promise<IFileData> => {
    const fileName = `${directory}/${chapter}`;
    const { stripHeadlinesAndFormatsFrom = [] } = metadata;

    return await new Promise((resolve, reject): void => {
        const prepareFile = (
            err: NodeJS.ErrnoException | null,
            fileDataBuffer: Buffer
        ): void => {
            let fileData = fileDataBuffer.toString();
            if (!isNil(err)) {
                reject(err);
            } else {
                fileData = resolveConditionals(fileData, bookType);
                fileData = resolveReferences(fileData, directory);
                fileData = resolveVariables(fileData, metadata);
                fileData = stripFrontMatter(fileData);
                if (stripHeadlinesAndFormatsFrom.includes(bookType)) {
                    fileData = stripTitelsAndFormats(fileData);
                }
                resolve({ len: fileData.length, fileName, fileData });
            }
        };

        fs.readFile(fileName, {}, prepareFile);
    });
};

const stripFrontMatter = (text: string): string => {
    const frontMatterRegex = /^---\s*\n([\s\S]*?)\n---\s*\n?([\s\S]*)/;
    const match = text.match(frontMatterRegex);

    if (match) {
        return match[2];
    } else {
        return text;
    }
};

const resolveReferences = (text: string, directory: string): string => {
    const re = /(<div.*class=".*\b(infobox)\b.*".*id="(.*)".*>)</gm;
    let ref;
    const references = [];

    while ((ref = re.exec(text)) !== null) {
        references.push({ replace: ref[1], type: ref[2], content: ref[3] });
    }

    for (let i = 0; i < references.length; i++) {
        const { replace, type, content } = references[i];

        const fileName = `${directory}/dictionary/${content}.yaml`;
        const yamlText = fs.readFileSync(fileName).toString();
        let referenceText: {
            novel: string;
        };
        try {
            referenceText = YAML.parse(yamlText);
        } catch (err) {
            const error = err as Error;
            console.error(
                chalk.red(
                    `Can't create YAML text from '${fileName}', because of '${error.message}'`
                )
            );
            referenceText = {
                novel: 'There was an error reading this box.'
            };
        }

        switch (type) {
            case 'infobox':
                text = text.replace(
                    replace,
                    `${replace}${referenceText.novel}`
                );
        }
    }

    return text;
};

const resolveVariables = (text: string, metadata: Metadata): string => {
    // const projectRoot : string = findProjectRoot(process.cwd(), {maxDepth: 12})
    // || 'unknown';
    const projectRoot: string = process.cwd();

    const resolveData: MarkdownVariables = {
        BOOK_TITLE: metadata.title,
        BOOK_VERSION: metadata.version,
        BITBUCKET_COMMIT:
            metadata.commit !== undefined ? metadata.commit : 'snapshot',
        BITBUCKET_BUILD_NUMBER:
            metadata.build !== undefined ? metadata.build : 'x',
        BOOK_REPOSITORY:
            metadata.repository !== undefined ? metadata.repository : '_',
        BOOK_LICENSE: metadata.license,
        BOOK_ASIN: metadata.asin,
        BOOK_ISBN: metadata.isbn,
        MEDIA_PATH: `${projectRoot}/media`,
        RAW_BOOK_URL:
            metadata.rawBookUrl !== undefined ? metadata.rawBookUrl : ''
    };

    return replaceVariables(text, resolveData);
};

const resolveConditionals = (text: string, bookType: EbookType): string => {
    let targetText: string = text;
    const re = /<!-- if booktype is ([\w,]+) -->(.*)<!-- endif booktype -->/g;
    let ref: RegExpExecArray | null;

    while ((ref = re.exec(text)) !== null) {
        const [completeText, bookTypes, replacementText] = ref;

        targetText = targetText.replace(
            completeText,
            bookTypes.includes(bookType) ? replacementText : ''
        );
    }

    return targetText;
};

const stripTitelsAndFormats = (text: string): string => {
    let targetText: string = text;
    const reStrip: RegExp[] = [
        /#{1,3}.*?\n/g, // Strip markdown headlines
        /<div.*class="impressum".*?<\/div>/gs
    ]; // strip impressum
    let ref: RegExpExecArray | null;

    reStrip.forEach((re) => {
        while ((ref = re.exec(text)) !== null) {
            const [completeText] = ref;

            targetText = targetText.replace(completeText, '');
        }
    });

    return targetText;
};

const prepareBook = async (
    directory: string,
    config: BookProductionOptions,
    bookTypes: EbookType[],
    parameter: AdditionalParameter
): Promise<ISourceFileNames> => {
    const chapters = await readChapters(directory);
    const srcFileNames: ISourceFileNames = {};

    const metadata = {
        ...parameter,
        ...config.metadata,
        ...config.additionalMetadata
    };

    for (const bookType of bookTypes) {
        const bookData = await Promise.all(
            chapters.map(
                async (chapter) =>
                    await prepareFile(chapter, directory, metadata, bookType)
            )
        );
        const bookText = bookData.reduce(
            (acc: string, data: IFileData) => `${acc}\n${data.fileData}`,
            ''
        );

        // ... convert book for every type TODO: change prepareFile to replace if/endif
        // structures

        srcFileNames[
            bookType
        ] = `${TEMP_DIR}/ebookmaker_tmpfile.${bookType}.md`;

        fs.writeFileSync(srcFileNames[bookType], bookText);
    }

    if (fs.existsSync(`${TEMP_DIR}/${MEDIA_DIR}`)) {
        fs.unlinkSync(`${TEMP_DIR}/${MEDIA_DIR}`);
    }
    fs.symlinkSync(`${PROJECT_ROOT}/${MEDIA_DIR}`, `${TEMP_DIR}/${MEDIA_DIR}`);

    return srcFileNames;
};

export default prepareBook;
