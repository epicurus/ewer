import {
    BookProductionOptions,
    EbookType,
    AdditionalParameter
} from '../../types';

import uploadBook, { getPublicPath } from './uploadBook';
import prepareBook from './prepareBook';
import prepareOptions from './prepareOptions';
import convertBook from './convertBook';
import pdfPostProduction from './pdfPostProduction';

const createBooks = async (
    directory: string,
    config: BookProductionOptions,
    language: string,
    bookTypes: EbookType[],
    parameter: AdditionalParameter
): Promise<void> => {
    await prepareOptions(config);
    const {
        lookAndFeel,
        txtInputOptions,
        pdfOutputOptions,
        epubOutputOptions,
        azw3OutputOptions,
        txtOutputOptions,
        rtfOutputOptions,
        tableOfContents,
        general: { bookTypesToUpload },
        additionalMetadata: { rawBookUrl }
    } = config;

    const bookName = `${config.metadata.title.replace(
        /[\s]/g,
        ''
    )}_${language}`;
    if (rawBookUrl !== undefined && !rawBookUrl?.startsWith('http')) {
        config.additionalMetadata.rawBookUrl = bookTypesToUpload
            .map((bookType) => `${getPublicPath()}/${bookName}.${bookType}`)
            .join(', ');
    }
    const srcFileNames = await prepareBook(
        directory,
        config,
        bookTypes,
        parameter
    );

    console.log(`Language: =  ${language}  = = = = = = = = = = = =`);

    for (const bookType of bookTypes) {
        const specificOptions =
            bookType === 'pdf'
                ? { ...pdfOutputOptions, ...tableOfContents }
                : bookType === 'azw3'
                ? { ...azw3OutputOptions, ...tableOfContents }
                : bookType === 'epub'
                ? { ...epubOutputOptions, ...tableOfContents }
                : bookType === 'txt'
                ? { ...txtOutputOptions }
                : bookType === 'rtf'
                ? { ...rtfOutputOptions }
                : {};

        const options = {
            language,
            ...config.metadata,
            ...lookAndFeel,
            ...txtInputOptions,
            ...specificOptions
        };
        const bookPath = await convertBook(
            srcFileNames[bookType],
            bookName,
            options,
            bookType
        );
        if (bookTypesToUpload.includes(bookType)) {
            await uploadBook(bookPath);
        }

        if (bookType === 'pdf') {
            await pdfPostProduction(bookPath, pdfOutputOptions);
        }
    }
};

export default createBooks;
