import fs from 'fs';
import { isNil } from 'lodash';
import path from 'path';
import * as readline from 'readline';
import { replaceVariables, IResolveData } from '../common';
import { paramCase } from 'change-case';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const question = async (question: string): Promise<string> =>
    await new Promise((resolve) => rl.question(question, resolve));

const greeting = (options: {
    projectName: string;
    mainLanguage: string;
}): string => `
I created the book project.

Your next steps could be ...

cd ${options.projectName}
cp <PATH TO MY MARKDOWN FILES>/*.md ${options.mainLanguage}
yarn build:${options.mainLanguage}

Then you will have the ebook version of your text.
`;

const copyFileSyncWithReplacements = (
    source: string,
    destination: string,
    resolveData: IResolveData
): void => {
    const text = fs.readFileSync(source).toString();
    fs.writeFileSync(destination, replaceVariables(text, resolveData));
};

const initProject = async (): Promise<void> => {
    const projectNameAnswer: string = await question(
        'Project name? [MyFirstBook] '
    );
    const projectName: string = !isNil(projectNameAnswer)
        ? projectNameAnswer
        : 'sample';
    const npmProjectName: string = paramCase(projectName);
    const bookTitleAnswer: string = await question(
        'Book title? [My first Book] '
    );
    const bookTitle: string = !isNil(bookTitleAnswer)
        ? bookTitleAnswer
        : 'sample';
    const authorAnswer: string = await question(
        'Author? [Firstname Lastname] '
    );
    const author: string = !isNil(authorAnswer) ? authorAnswer : 'No name';
    const mainLanguageAnswer: string = await question('Main language? [en] ');
    const mainLanguage: string = !isNil(mainLanguageAnswer)
        ? mainLanguageAnswer
        : 'en';
    const licenseAnswer: string = await question('License? [UNLICENSED] ');
    const license: string = !isNil(licenseAnswer)
        ? licenseAnswer
        : 'UNLICENSED';
    const resolveData: IResolveData = {
        BOOK_TITLE: bookTitle,
        PROJECT_NAME: npmProjectName,
        PROJECT_DIR: `${process.cwd()}/${projectName}`,
        AUTHOR: author,
        LICENSE: license,
        MAIN_LANGUAGE: mainLanguage
    };
    console.assert(
        ['en', 'de', 'se'].includes(mainLanguage),
        'Main language must be one of en | de | se'
    );

    const templatePath = path.join(__dirname, '..', 'Templates');
    const projectPath = `${process.cwd()}/${projectName}`;
    try {
        fs.mkdirSync(projectPath);
        fs.mkdirSync(`${projectPath}/text`);
        fs.mkdirSync(`${projectPath}/text/${mainLanguage}`);
        fs.mkdirSync(`${projectPath}/text/${mainLanguage}/dictionary`);
        fs.mkdirSync(`${projectPath}/audio`);
        fs.mkdirSync(`${projectPath}/audio/${mainLanguage}`);
        fs.mkdirSync(`${projectPath}/audio/${mainLanguage}/transcribe`);
        fs.mkdirSync(`${projectPath}/media`);
        fs.mkdirSync(`${projectPath}/media/fonts`);
        fs.mkdirSync(`${projectPath}/original`);

        fs.copyFileSync(
            `${templatePath}/package.json`,
            `${projectPath}/package.json`
        );
        fs.copyFileSync(
            `${templatePath}/bitbucket-pipelines.yml`,
            `${projectPath}/bitbucket-pipelines.yml`
        );
        fs.copyFileSync(
            `${templatePath}/global.css`,
            `${projectPath}/media/global.css`
        );
        fs.copyFileSync(
            `${templatePath}/template.md`,
            `${projectPath}/media/template.md`
        );
        fs.copyFileSync(
            `${templatePath}/IndieFlower-Regular.ttf`,
            `${projectPath}/media/fonts/IndieFlower-Regular.ttf`
        );
        fs.copyFileSync(
            `${templatePath}/00-Impressum.md`,
            `${projectPath}/text/${mainLanguage}/00-Impressum.md`
        );
        fs.copyFileSync(
            `${templatePath}/01-page-example.md`,
            `${projectPath}/text/${mainLanguage}/01-page-example.md`
        );
        fs.copyFileSync(
            `${templatePath}/dictionary-example.yaml`,
            `${projectPath}/text/${mainLanguage}/dictionary/dictionary-example.yaml`
        );
        fs.copyFileSync(
            `${templatePath}/package.json`,
            `${projectPath}/package.json`
        );
        fs.copyFileSync(
            `${templatePath}/gitignore`,
            `${projectPath}/.gitignore`
        );

        copyFileSyncWithReplacements(
            `${templatePath}/package.json`,
            `${projectPath}/package.json`,
            resolveData
        );
        copyFileSyncWithReplacements(
            `${templatePath}/config.js`,
            `${projectPath}/config.js`,
            resolveData
        );

        console.info(
            greeting({
                projectName,
                mainLanguage
            })
        );
    } catch (err) {
        const error = err as Error;
        console.error(error.message);
        process.exit(1);
    }
};

export default initProject;
