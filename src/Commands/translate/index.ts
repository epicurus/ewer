import fs from 'fs';
import { isNil, get } from 'lodash';
import deeplApi from '../../Components/TranslationAi/DeeplApi';
import getProductionOptions from '../../common/getProductionOptions';
import type { SupportedLanguages } from '../../types';
import translateFile from './translateFile';
import translateDir from './translateDir';
import isDirectory from './isDirectory';

let apiReady = false;

type Props = {
    sourceLanguage: string;
    targetLanguage: string;
    sourceFile: string;
    destFile?: string;
};

export const listLanguages = async (): Promise<SupportedLanguages> => {
    await apiReady;
    return deeplApi.listLanguages();
};

const translate = async ({
    sourceLanguage,
    targetLanguage,
    sourceFile = '.',
    destFile
}: Props) => {
    if (apiReady === false) {
        await deeplApi.init();
        apiReady = true;
    }

    const options = await getProductionOptions();
    const defaultLanguageCodes = get(
        options,
        'translation.defaultLanguageCodes',
        {}
    ) as { [key: string]: string };

    targetLanguage = defaultLanguageCodes[targetLanguage] || targetLanguage;

    if (deeplApi.isValidTargetLanguage(targetLanguage) === false) {
        console.error(`Target language '${targetLanguage}' is invalid.`);
        process.exit(1);
    }

    if (!isNil(sourceLanguage)) {
        if (deeplApi.isValidSourceLanguage(sourceLanguage) === false) {
            console.error(`Source language '${sourceLanguage}' is invalid.`);
            process.exit(1);
        }

        if (!isDirectory(sourceFile)) {
            console.error(
                `Source language can only be specified for directories.`
            );
            process.exit(1);
        }

        const sourceDir = `${sourceFile}/${sourceLanguage}`;
        if (!isDirectory(sourceDir)) {
            console.error(
                `I didn't find the source language directory '${sourceDir}.'`
            );
            process.exit(1);
        }

        const destDir = isNil(destFile)
            ? `${sourceFile}/${targetLanguage.substring(0, 2)}`
            : `${destFile}/${targetLanguage.substring(0, 2)}`;

        if (!isDirectory(destDir)) {
            await fs.promises.mkdir(destDir).catch(() => {
                console.error(`Target is not a directoy: '${destDir}.'`);
                process.exit(1);
            });
        }

        await translateDir({
            sourceDir,
            destDir,
            recursive: true,
            targetLanguage
        });
    }

    if (!isDirectory(sourceFile)) {
        await translateFile({
            sourceFile,
            destFile,
            targetLanguage
        });
    } else {
        if (!isNil(destFile) && !isDirectory(destFile)) {
            console.error(`Target is not a directoy: '${destFile}.'`);
            process.exit(1);
        }

        await translateDir({
            sourceDir: sourceFile,
            destDir: isNil(destFile) ? sourceFile : destFile,
            recursive: false,
            targetLanguage
        });
    }
};

export default translate;
