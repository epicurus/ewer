import * as deepl from 'deepl-node';
import fs from 'node:fs';
import path from 'path';
import { get, isNil } from 'lodash';
import getProductionOptions from '../../common/getProductionOptions';
import DeeplApi from '../../Components/TranslationAi/DeeplApi';
import HugoFile from '../../Components/HugoUtils';
import { markdownFileExt } from '../../constants';
import isDirectory from './isDirectory';

type Props = {
    sourceFile: string;
    destFile?: string;
    targetLanguage: string;
};

const translateFile = async ({
    sourceFile,
    destFile,
    targetLanguage
}: Props): Promise<string> => {
    const options = await getProductionOptions();
    const fileExtensions = get(
        options,
        'translation.fileExtensions',
        []
    ) as string[];

    const validExtention =
        fileExtensions.reduce(
            (acc, ext) => sourceFile.endsWith(ext) || acc,
            false
        ) === true;

    if (!validExtention) {
        console.error(
            `File extension of ${sourceFile} is not one of ${markdownFileExt.join(
                ', '
            )}`
        );
        process.exit(1);
    }

    const sourcePath = path.resolve(sourceFile);
    const sourceName = path.basename(sourcePath);
    const destName = sourceName;
    let destPath = isNil(destFile)
        ? sourcePath
        : isDirectory(destFile) === true
        ? `${path.resolve(destFile)}/${destName}`
        : path.resolve(destFile);

    if (sourcePath === destPath) {
        destPath = `${path.dirname(destPath)}/${targetLanguage}-${destName}`;
    }

    const text = fs.readFileSync(sourcePath).toString();

    const hugoFile = new HugoFile(text, options);
    const { translatableText, translatableFrontMatter } = hugoFile;

    const mainText = await DeeplApi.translateMarkdown(
        [translatableText],
        targetLanguage as deepl.TargetLanguageCode,
        null
    );

    const detectedLanguage = mainText[0].detectedSourceLang;
    const sourceText = hugoFile.sourceText;

    if (detectedLanguage === targetLanguage) {
        console.warn(
            `The detected language was the same as the target language (${targetLanguage}), so I did nothing. `
        );
        return sourceText;
    }

    const frontMatter = await DeeplApi.translateMarkdown(
        translatableFrontMatter,
        targetLanguage as deepl.TargetLanguageCode,
        detectedLanguage
    );

    const destText = hugoFile.getDestText(
        mainText[0].text,
        frontMatter.map((t) => t.text)
    );

    console.info(
        `Translated ${sourceFile} (${detectedLanguage}) to ${destFile} (${targetLanguage})`
    );
    fs.writeFileSync(sourcePath, sourceText);

    await fs.promises
        .mkdir(path.dirname(destPath), { recursive: true })
        .catch((err) => {
            const error = err as Error;

            console.error(`Error while making directory: ${error.message}`);
        });

    fs.writeFileSync(destPath, destText);

    return destText;
};

export default translateFile;
