import fs from 'fs';
import path from 'path';
import { get, isNil } from 'lodash';
import getProductionOptions from '../../common/getProductionOptions';
import HugoFile from '../../Components/HugoUtils';
import translateFile from './translateFile';

type Props = {
    sourceDir: string;
    destDir: string;
    recursive: boolean;
    targetLanguage: string;
};

function searchFiles(
    dir: string,
    pattern: RegExp,
    recursive: boolean
): string[] {
    let results: string[] = [];
    const files = fs.readdirSync(dir);

    for (const file of files) {
        const filePath = path.join(dir, file);
        const stat = fs.statSync(filePath);

        if (recursive === true && stat.isDirectory()) {
            results = results.concat(searchFiles(filePath, pattern, true));
        } else if (pattern.test(filePath)) {
            results.push(filePath);
        }
    }

    return results;
}

const translateDir = async ({
    sourceDir,
    destDir,
    recursive,
    targetLanguage
}: Props) => {
    const options = await getProductionOptions();
    const fileExtensions = get(
        options,
        'translation.fileExtensions',
        []
    ) as string[];

    const pattern = new RegExp(`\.(${fileExtensions.join('|')})$`, 'i');

    const sourceFiles = searchFiles(sourceDir, pattern, recursive);

    for (const sourceFile of sourceFiles) {
        const relativePath = sourceFile.substring(sourceDir.length + 1);

        const destFile = `${destDir}/${relativePath}`;
        let text = '';
        try {
            text = fs.readFileSync(destFile).toString();

            const options = await getProductionOptions();
            const hugoFile = new HugoFile(text, options);
            const { frontMatterVars } = hugoFile;

            if (
                !isNil(frontMatterVars.translation) &&
                frontMatterVars.translation !== 'auto'
            ) {
                continue;
            }
        } catch (err) {
            const error = err as NodeJS.ErrnoException;

            if (error.code !== 'ENOENT') {
                continue;
            }
        }

        if (text !== '') {
            const sourceStat = fs.statSync(sourceFile);
            const destStat = fs.statSync(destFile);

            const sourceTime = sourceStat.mtime.getTime();
            const destTime = destStat.mtime.getTime();

            if (sourceTime <= destTime) {
                continue;
            }
        }

        await translateFile({
            sourceFile,
            destFile,
            targetLanguage
        });
    }
};

export default translateDir;
