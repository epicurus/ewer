import fs from 'fs';

const isDirectory = (file: string): boolean => {
    try {
        const stat = fs.statSync(file);

        return stat.isDirectory() === true;
    } catch (err) {
        const error = err as NodeJS.ErrnoException;

        if (error.code === 'ENOENT') {
            return false;
        }

        console.error(error.message);
        process.exit(1);
    }
};

export default isDirectory;
