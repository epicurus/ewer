import transcribeDirectory from './transcribeDirectory';

const transcribeAudioBook = async (
    language: string,
    audioPath: string,
    textPath: string
): Promise<void> => {
    await transcribeDirectory(language, audioPath, textPath);
};

export default transcribeAudioBook;
