import fs from 'fs';
import path from 'path';
import { isNil } from 'lodash';
import { FileDescriptor } from '../../types';
import { GoogleCloudSpeechApi } from '../../Components/SpeechAi';
import readMediaFiles from './readMediaFiles';

const MAX_PARALLEL_REQUESTS = 1;

const EPIKURETS_TYPES = ['epicurus', 'glossary', 'thought'];

const MARKDOWN_TEMPLATE = `
## {TITLE}

[//]: # (epikuretId:{EPIKURETID})
[//]: # (title:{TITLE})
[//]: # (author:{AUTHOR})
[//]: # (type:{TYPE})
[//]: # (audio:{AUDIO})
[//]: # (video:{VIDEO})
[//]: # (language:{LANGUAGE})
[//]: # (published:false)
[//]: # (promotion:none)

{TEXT}
`;

const textToMarkdown = (
    mediaFile: FileDescriptor,
    text: string,
    type: string
): string => {
    if (isNil(mediaFile)) {
        return '';
    }

    const templatePath = `${process.cwd()}/media/template.md`;
    const {
        name,
        publicAudioUrl,
        publicVideoUrl,
        epikuretId,
        author,
        title,
        languageCode
    } = mediaFile;

    let template;
    try {
        template = fs.readFileSync(templatePath).toString();
    } catch (_) {
        template = MARKDOWN_TEMPLATE;
    }
    return template
        .replace(/\{TITLE}/g, title ?? name ?? '')
        .replace(/\{TEXT}/g, text ?? '')
        .replace(/\{EPIKURETID}/g, epikuretId ?? '')
        .replace(/\{AUTHOR}/g, author ?? '')
        .replace(/\{TYPE}/g, type)
        .replace(/\{AUDIO}/g, publicAudioUrl ?? '')
        .replace(/\{VIDEO}/g, publicVideoUrl ?? '')
        .replace(/\{LANGUAGE}/g, languageCode ?? '');
};

const writeText = async (
    text: string,
    language: string,
    mediaFile: FileDescriptor,
    textPath: string
): Promise<void> => {
    if (isNil(mediaFile)) {
        return;
    }

    let version = 0;
    let destPath = '';
    while (destPath === '') {
        const versionSuffix =
            version > 0 ? `-${(100 + version).toString().substr(-2)}` : '';
        destPath = `${process.cwd()}/${textPath}/${
            mediaFile.name
        }${versionSuffix}.md`;
        if (fs.existsSync(destPath)) {
            destPath = '';
        }
        version++;
    }

    const type = EPIKURETS_TYPES.reduce(
        (acc: string | null, type: string) =>
            acc !== null && destPath.toLowerCase().includes(`/${type}/`)
                ? type
                : null,
        null
    );

    const mdText = textToMarkdown(
        mediaFile,
        text,
        type !== null ? type : 'unknown'
    );
    if (destPath !== '') {
        fs.writeFileSync(destPath, mdText);
    }
};

const transcribeDirectory = async (
    language: string,
    audioPath = language,
    textPath: string = audioPath
): Promise<FileDescriptor[] | null> => {
    const speechApi = new GoogleCloudSpeechApi(language);
    const normalizedPath = path.normalize(`${process.cwd()}/${audioPath}`);
    const mediaFiles = await readMediaFiles(language, normalizedPath);
    const epikuretId = normalizedPath.slice(
        normalizedPath.lastIndexOf('epikurets/') + 'epikurets/'.length
    );

    let mediaFilesToProcess;
    const mediaFilesProcessed: FileDescriptor[] = [];

    if (mediaFiles.length === 0) {
        console.info(
            'I did not find files to transcribe (maybe all are already transcribed).'
        );
    }
    do {
        mediaFilesToProcess = mediaFiles.splice(0, MAX_PARALLEL_REQUESTS);
        if (mediaFilesToProcess.length === 0) {
            break;
        }

        const processed: FileDescriptor[] = await Promise.all(
            mediaFilesToProcess.map(
                async (mediaFile: FileDescriptor): Promise<FileDescriptor> => {
                    if (!isNil(mediaFile)) {
                        mediaFile.epikuretId = epikuretId;
                        if (mediaFile.shouldTranscribe === true) {
                            const { transcription: text } =
                                await speechApi.convertSpeechToText(mediaFile);

                            await writeText(
                                text,
                                language,
                                mediaFile,
                                textPath
                            );
                        } else if (mediaFile.shouldCreateMarkdown === true) {
                            await writeText('', language, mediaFile, textPath);
                        }
                    }

                    return mediaFile;
                }
            )
        );
        mediaFilesProcessed.push(...processed);
    } while (mediaFiles.length > 0);

    return mediaFilesProcessed;
};

export default transcribeDirectory;
