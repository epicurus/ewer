import fs from 'fs';
import { isNil } from 'lodash';
import {
    LANGUAGE_CODES,
    AUDIO_FILE_TYPES,
    VIDEO_FILE_TYPES
} from '../../common';
import { FileDescriptor } from '../../types';
import { GoogleCloudStorageFileIo } from '../../Components/RemoteFileIo';

const FileIo = new GoogleCloudStorageFileIo({ bucketName: 'ewer' });

const getLanguageCode = (language: string): string => {
    return LANGUAGE_CODES[language] ?? 'en-GB';
};

const capitalizeFirst = (str: string): string => {
    return str !== ''
        ? `${str.charAt(0).toUpperCase()}${str.substr(1).toLowerCase()}`
        : '';
};

const readMediaFiles = async (
    language: string,
    audioPath: string
): Promise<FileDescriptor[]> => {
    let mediaFiles;
    const fileTypes = [...AUDIO_FILE_TYPES, ...VIDEO_FILE_TYPES];
    const re = new RegExp(
        `^(.*).(${fileTypes.map((fileType) => `${fileType}`).join('|')})$`
    );
    try {
        mediaFiles = fs
            .readdirSync(audioPath)
            .map((file: string) => {
                const match = file.match(re);
                const fileName: FileDescriptor = {
                    name: match !== null ? match[1] : '',
                    ext: match !== null ? match[2] : ''
                };

                return fileName.name !== '' ? fileName : null;
            })
            .sort((a, b) => {
                return !isNil(a) && !isNil(b) && a.ext < b.ext ? -1 : 1;
            })
            .reduce(
                (
                    acc: FileDescriptor[],
                    file: FileDescriptor | null,
                    _: unknown,
                    fileNames: FileDescriptor[]
                ) => {
                    if (isNil(file)) {
                        return acc;
                    }

                    const languageCode = (file.languageCode =
                        getLanguageCode(language));

                    if (languageCode === '') {
                        return acc;
                    }

                    const mediaPath = `${audioPath}/${file.name}.${file.ext}`;
                    const mdPath = `${audioPath}/${file.name}.md`;
                    const isAudioFile = AUDIO_FILE_TYPES.includes(file.ext);
                    const mdFileExists = fs.existsSync(mdPath);
                    const { mtime: mediaMtime } = fs.statSync(mediaPath);
                    const { mtime: mdMtime = '' } = mdFileExists
                        ? fs.statSync(mdPath)
                        : {};

                    const titleRe = /`(.*)`/;
                    const authorRe = /\[(.*)\]/;
                    const [, title] = file.name.match(titleRe) ?? [];
                    const [, author] = file.name.match(authorRe) ?? [];

                    file.title = title ?? '';
                    file.author = !isNil(author) ? capitalizeFirst(author) : '';

                    if (isAudioFile) {
                        const videoFile = fileNames.find(
                            (fileName) =>
                                !isNil(fileName) &&
                                VIDEO_FILE_TYPES.includes(fileName.ext) &&
                                file.name === fileName.name
                        );

                        if (!isNil(videoFile)) {
                            file.videoExt = videoFile.ext;
                        }

                        if (!mdFileExists) {
                            file.shouldTranscribe = true;
                            file.shouldUpload = true;
                        }

                        if (mdMtime !== '' && mediaMtime > mdMtime) {
                            file.shouldUpload = true;
                        }

                        return file.shouldUpload === true ||
                            file.shouldTranscribe === true
                            ? [...acc, file]
                            : acc;
                    } else {
                        const audioFile = fileNames.find(
                            (fileName) =>
                                !isNil(fileName) &&
                                AUDIO_FILE_TYPES.includes(fileName.ext) &&
                                file.name === fileName.name
                        );

                        if (!isNil(audioFile)) {
                            return acc;
                        }

                        file.videoExt = file.ext;
                        file.ext = '';

                        if (!mdFileExists) {
                            file.shouldCreateMarkdown = true;
                        }

                        if (!isNil(mdMtime) && mediaMtime > mdMtime) {
                            file.shouldUpload = true;
                        }

                        return file.shouldCreateMarkdown === true ||
                            file.shouldUpload === true
                            ? [...acc, file]
                            : acc;
                    }
                },
                []
            );
    } catch (err) {
        const error = err as Error & { code: string };

        if (error.code === 'ENOENT') {
            console.error(
                `I don't find audio for language '${language}' files at '${audioPath}'`
            );
            return [];
        }

        throw error;
    }

    if (mediaFiles.length === 0) {
        return [];
    }

    console.info(`Processing ${audioPath}:`);

    for (const mediaFile of mediaFiles) {
        if (
            isNil(mediaFile) ||
            (mediaFile.shouldUpload !== true &&
                mediaFile.shouldCreateMarkdown !== true)
        ) {
            continue;
        }

        if (mediaFile.ext !== '') {
            const sourcePath = `${audioPath}/${mediaFile.name}.${mediaFile.ext}`;
            const result = await FileIo.upload(sourcePath, { public: true });
            if (result === null) {
                throw new Error(`Cannot upload file '${sourcePath}'`);
            }

            const { internalUrl, publicUrl } = result;
            mediaFile.url = internalUrl;
            mediaFile.publicAudioUrl = publicUrl;
            console.info(`Uploaded audio file to ${publicUrl}`);
        }

        if (!isNil(mediaFile.videoExt)) {
            const sourcePath = `${audioPath}/${mediaFile.name}.${
                mediaFile.videoExt ?? 'unknown'
            }`;
            const result = await FileIo.upload(sourcePath, { public: true });
            if (result === null) {
                throw new Error(`Cannot upload file '${sourcePath}'`);
            }
            const { publicUrl } = result;
            mediaFile.publicVideoUrl = publicUrl;
            console.info(`Uploaded video file to ${publicUrl}`);
        }
    }

    return mediaFiles;
};

export default readMediaFiles;
