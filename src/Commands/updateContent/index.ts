/* eslint-disable no-await-in-loop, no-restricted-syntax */
import fs from 'fs';
import path from 'path';
import sha1 from 'sha1';
import { isNil } from 'lodash';
import { EpikuretEntry, EpikuretType, BuyProp } from '../../types';
import {
    epicurusModule,
    thoughtModule,
    bookModule,
    glossaryModule
} from './modules';
import { setConfig } from './config';

const epikuretPropNames = [
    'epikuretId',
    'title',
    'author',
    'type',
    'audio',
    'video',
    'origin',
    'year',
    'language',
    'published',
    'promotion',
    'book',
    'audiobook',
    'ebook',
    'cover',
    'download',
    'buy'
] as const;

type IEpikuretProps = Record<string, string | BuyProp>;

type IEpikuretI18n = Record<string, EpikuretEntry[]>;

const getEpikuretEntry = async (
    filename: string
): Promise<EpikuretEntry | null> => {
    const markdownFile: string = fs.readFileSync(filename, {
        encoding: 'utf-8'
    });
    const epikuretProps: IEpikuretProps = {};

    epikuretPropNames.forEach((prop) => {
        const reExpression = `# [('"]${prop}:([^)'"]+)[)'"]`;
        const reG = new RegExp(reExpression, 'g');
        const matchAll = markdownFile.match(reG);

        if (matchAll !== null && matchAll?.length !== 0) {
            const re = new RegExp(reExpression);
            matchAll.forEach((matchStr) => {
                const match = matchStr.match(re);
                if (!isNil(match)) {
                    const [, p] = match;

                    if (prop === 'buy') {
                        const match = p.match(/^([^:]+):([^:]+):(.*)$/);
                        if (!isNil(match)) {
                            const [, type, provider, url] = match;

                            const buyProp = (epikuretProps[
                                prop
                            ] as BuyProp) ?? { [type]: [] };

                            buyProp[type].push({ provider, url });
                            epikuretProps[prop] = buyProp;
                        }
                    } else {
                        epikuretProps[prop] = p;
                    }
                }
            });
        }
    });

    const { epikuretId, title, language, type, published, buy } = epikuretProps;

    if (!isNil(epikuretId)) {
        const epikuretEntry: EpikuretEntry = {
            ...epikuretProps,
            epikuretId: epikuretId as string,
            title: title as string,
            type: type as EpikuretType,
            published: published === 'true',
            buy: buy as BuyProp
        };

        return type === 'epicurus'
            ? await epicurusModule(
                  epikuretEntry,
                  language as string,
                  filename,
                  markdownFile
              )
            : type === 'thought'
            ? await thoughtModule(
                  epikuretEntry,
                  language as string,
                  filename,
                  markdownFile
              )
            : type === 'book'
            ? await bookModule(
                  epikuretEntry,
                  language as string,
                  filename,
                  markdownFile
              )
            : type === 'glossary'
            ? glossaryModule(
                  epikuretEntry,
                  language as string,
                  filename,
                  markdownFile
              )
            : null;
    }

    return null;
};

const readEpikuretEntries = async (
    storagePath: string
): Promise<EpikuretEntry[]> => {
    const epikuretEntries: EpikuretEntry[] = [];

    const handleDirectory = async (storagePath: string): Promise<void> => {
        const dir = fs.readdirSync(storagePath, { withFileTypes: true });
        const markdownFiles = dir.filter((dirent) =>
            dirent.name.endsWith('.md')
        );
        const directories = dir.filter((dirent) => dirent.isDirectory());

        for (const dirent of markdownFiles) {
            const epikuretEntry = await getEpikuretEntry(
                `${storagePath}/${dirent.name}`
            );
            if (epikuretEntry?.published === true) {
                epikuretEntries.push(epikuretEntry);
            }
        }

        for (const dirent of directories) {
            await handleDirectory(`${storagePath}/${dirent.name}`);
        }
    };

    await handleDirectory(storagePath);

    return epikuretEntries;
};

const updateContent = async (
    storagePath: string,
    contentPath: string
): Promise<void> => {
    await setConfig(storagePath);
    const epikuretEntries = await readEpikuretEntries(storagePath);
    const epikuretsI18n: IEpikuretI18n = epikuretEntries.reduce(
        (acc: IEpikuretI18n, entry: EpikuretEntry) => {
            const { language } = entry;

            if (!isNil(language) && acc[language] !== undefined) {
                acc[language].push(entry);
            } else if (!isNil(language)) {
                acc[language] = [entry];
            }

            return acc;
        },
        {}
    );

    const importLines: string[] = [];
    for (const language of Object.keys(epikuretsI18n)) {
        const entries = epikuretsI18n[language];
        entries.forEach((entry) => {
            delete entry.language;
            delete entry.published;

            if (!isNil(entry.cover)) {
                const ext = path.extname(entry.cover);
                const absStoragePathLength = path.resolve(storagePath).length;
                const coverName = entry.cover.substr(absStoragePathLength);
                const cover = `${sha1(coverName) as string}${ext}`;
                const coverPath = `${contentPath}/images/${cover}`;
                if (!fs.existsSync(coverPath)) {
                    fs.copyFileSync(entry.cover, coverPath);
                }
                const coverVar = `epikuretImage${importLines.length}`;
                entry.cover = coverVar;
                importLines.push(
                    `import ${coverVar} from '../images/${cover}';`
                );
            }
        });
        const json = JSON.stringify(epikuretsI18n[language], null, 2);
        const jsonPath = path.normalize(
            `${process.cwd()}/${contentPath}/${language}/epikurets.ts`
        );

        let data = `${importLines.join('\n')}\nexport default ${json}`;
        data = data.replace(/"epikuretImage(\d+)",/g, 'epikuretImage$1,');
        fs.writeFileSync(jsonPath, data);

        console.log(
            `I wrote ${epikuretsI18n[language].length} epikurets for language ${language}.`
        );
    }
};

export default updateContent;
