import fs from 'fs';
import path from 'path';
import { isNil } from 'lodash';
import { EpikuretEntry } from '../../../types';
import { getConfig } from '../config';

const bookRe = /\n\n([^[#](?:(?!\n\n).)*)/gm;

export const bookModule = async (
    epikuretEntry: EpikuretEntry,
    language: string,
    filename: string,
    markdownFile: string
): Promise<EpikuretEntry> => {
    const { subtitleSnippets } = await getConfig();
    const matches = [...markdownFile.matchAll(bookRe)];
    const lines: string[] = [];
    for (const match of matches) {
        lines.push(match[1]);
    }

    const { epikuretId, cover, book, ebook, audiobook } = epikuretEntry;

    if (!isNil(cover) && !fs.existsSync(cover)) {
        const sanitizedCover = `${path.resolve(
            path.dirname(filename)
        )}/${cover}`;
        if (fs.existsSync(sanitizedCover)) {
            epikuretEntry.cover = sanitizedCover;
        } else {
            console.error(`Can't find image '${sanitizedCover}'. Skipping ...`);
            epikuretEntry.cover = undefined;
        }
    }

    ['year', 'cover'].forEach((prop: string) => {
        if (
            (epikuretEntry as unknown as Record<string, unknown>)[prop] !==
            undefined
        ) {
            console.error(
                `Prop '${prop}' is missing for epikuret '${epikuretId}' in language '${language}'.`
            );
        }
    });

    let subtitle = '';
    subtitle +=
        !isNil(subtitleSnippets) && !isNil(book)
            ? `${subtitleSnippets[language].book}, `
            : '';
    subtitle +=
        !isNil(subtitleSnippets) && !isNil(ebook)
            ? `${subtitleSnippets[language].ebook}, `
            : '';
    subtitle +=
        !isNil(subtitleSnippets) && !isNil(audiobook)
            ? `${subtitleSnippets[language].audiobook}, `
            : '';
    subtitle += `${epikuretEntry.year as string}`;

    epikuretEntry.subtitle = subtitle;
    epikuretEntry.thought = lines;

    return epikuretEntry;
};

export default bookModule;
