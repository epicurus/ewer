import { format, Locale } from 'date-fns';
import deLocale from 'date-fns/locale/de';
import enLocale from 'date-fns/locale/en-GB';
import svLocale from 'date-fns/locale/sv';
import { EpikuretEntry } from '../../../types';
import { getConfig } from '../config';

const dialogRe = /\n\n([^[#](?:(?!\n\n).)*)/gm;

type ILocales = {
    [key: string]: Locale;
};

const locales: ILocales = {
    'de-DE': deLocale,
    'en-GB': enLocale,
    'sv-SE': svLocale
};

export const epicurusModule = async (
    epikuretEntry: EpikuretEntry,
    language: string,
    filename: string,
    markdownFile: string
): Promise<EpikuretEntry> => {
    const { subtitleSnippets } = await getConfig();
    const matches = [...markdownFile.matchAll(dialogRe)];
    const lines: string[] = [];
    for (const match of matches) {
        lines.push(match[1]);
    }

    const date = filename.match(/\d{4}-\d{2}-\d{2}/);
    const gardenOfEpicurus =
        (subtitleSnippets && subtitleSnippets[language].gardenOfEpicurus) || '';
    const subtitle = `${gardenOfEpicurus}, ${
        date
            ? format(new Date(date[0]), 'd. MMM yyyy', {
                  locale: locales[language]
              })
            : ''
    }`;

    epikuretEntry.subtitle = subtitle;
    epikuretEntry.dialog = lines;

    return epikuretEntry;
};

export default epicurusModule;
