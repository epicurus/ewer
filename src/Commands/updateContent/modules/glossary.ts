import { format, Locale } from 'date-fns';
import deLocale from 'date-fns/locale/de';
import enLocale from 'date-fns/locale/en-GB';
import svLocale from 'date-fns/locale/sv';
import { EpikuretEntry } from '../../../types';

const dialogRe = /\n\n([^[#](?:(?!\n\n).)*)/gm;

export const glossaryModule = (
    epikuretEntry: EpikuretEntry,
    language: string,
    filename: string,
    markdownFile: string
): EpikuretEntry => {
    const matches = [...markdownFile.matchAll(dialogRe)];
    const lines: string[] = [];
    for (const match of matches) {
        lines.push(match[1]);
    }

    if (lines.length === 1 && lines[0].length < 3) {
        lines.splice(0, 1);
    }

    epikuretEntry.dialog = lines;
    return epikuretEntry;
};

export default glossaryModule;
