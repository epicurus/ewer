import { isNil } from 'lodash';
import { EpikuretEntry } from '../../../types';
import { getConfig } from '../config';

const thoughtRe = /\n\n([^[#](?:(?!\n\n).)*)/gm;

export const thoughtModule = async (
    epikuretEntry: EpikuretEntry,
    language: string,
    filename: string,
    markdownFile: string
): Promise<EpikuretEntry> => {
    const { subtitleSnippets } = await getConfig();
    const matches = [...markdownFile.matchAll(thoughtRe)];
    const lines: string[] = [];
    const { origin: epikurestsOrigin } = epikuretEntry ?? {};
    for (const match of matches) {
        lines.push(match[1]);
    }

    ['origin', 'year'].forEach((prop: string) => {
        if (
            (epikuretEntry as unknown as Record<string, unknown>)[prop] !==
            undefined
        ) {
            console.error(
                `Prop '${prop}' is missing for epikuret '${epikuretEntry.epikuretId}' in language '${language}'.`
            );
        }
    });

    if (epikurestsOrigin !== undefined && !isNil(subtitleSnippets)) {
        const origin = 'placeholder';
        // const origin =
        //     subtitleSnippets !== null && !isNil(epikurestsOrigin) ?
        // 	subtitleSnippets?[language][epikurestsOrigin] : '';
        const subtitle = `${origin}, <YEAR>`;

        epikuretEntry.subtitle = subtitle;
        epikuretEntry.thought = lines;
    }

    return epikuretEntry;
};

export default thoughtModule;
