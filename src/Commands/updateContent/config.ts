import path from 'path';
import type { Config } from '../../types';

let currentConfig: Config = {};

export const setConfig = async (storagePath: string): Promise<void> => {
    const configPath = path.normalize(
        `${process.cwd()}/${storagePath}/source.config.js`
    );
    currentConfig = await import(configPath);
};

export const getConfig = async (): Promise<Config> => {
    return currentConfig;
};

export default getConfig;
