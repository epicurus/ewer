import fs from 'fs';
import { isNil } from 'lodash';

const defaultDirectory = 'i18n';

const i18nPrepare = (): void => {
    const projectRoot = process.cwd();
    const path = `${projectRoot}/${defaultDirectory}`;
    const fileNames: string[] = fs.readdirSync(path);
    const languages: string[] = fileNames.filter(
        (fileName) => !fileName.includes('.')
    );
    const languageFiles: Record<string, object> = {};
    languages.forEach((language: string) => {
        const jsonFiles: string[] = fs
            .readdirSync(`${path}/${language}`)
            .filter((fileName: string) => fileName.endsWith('.json'));
        jsonFiles.forEach((fileName) => {
            const file = fs.readFileSync(`${path}/${language}/${fileName}`);
            if (!isNil(languageFiles[fileName])) {
                languageFiles[fileName] = {
                    ...languageFiles[fileName],
                    [language]: JSON.parse(file.toString())
                };
            } else {
                languageFiles[fileName] = {
                    [language]: JSON.parse(file.toString())
                };
            }
        });
    });
    for (const languageFile in languageFiles) {
        const languageJson = JSON.stringify(
            languageFiles[languageFile],
            null,
            2
        );
        fs.writeFileSync(`${path}/${languageFile}`, languageJson);
        console.info(
            `Written ${languageFile} with ${Object.keys(
                languageFiles[languageFile]
            ).join(', ')}.`
        );
    }
};

export default i18nPrepare;
