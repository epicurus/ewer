export const defaultSourceLanguage = 'en';
export const defaultTargetLanguage = 'en-GB';
export const markdownFileExt = ['.md', '.markdown'];
