#!/usr/bin/env node
import dotenv from 'dotenv';

import fs from 'fs';
import { isNil } from 'lodash';
import { Command } from 'commander';
import createBooks from './Commands/createBooks';
import transcribeAudioBook from './Commands/transcribeAudioBook';
// import updateContent from './Commands/updateContent';
import translate, { listLanguages } from './Commands/translate';
import getProjectRoot from './common/getProjectRoot';
import getProductionOptions from './common/getProductionOptions';

import { EbookType, BookProductionOptions } from './types';
import { VALID_EBOOK_TYPES } from './common';
import { version } from './package.json';
import { defaultSourceLanguage, defaultTargetLanguage } from './constants';

dotenv.config();

const program = new Command();

const directoryOfLanguage = (projectRoot: string, language: string): string => {
    const directory = `${projectRoot}/text/${language}`;

    if (!fs.existsSync(directory)) {
        throw new Error(`Directory ${directory} does not exist.`);
    }

    return directory;
};

const build = async (
    projectRoot: string,
    config: BookProductionOptions,
    language: string,
    bookTypes: EbookType[],
    build: string,
    commit: string
): Promise<void> => {
    console.assert(
        language !== '' && ['de', 'en', 'se'].includes(language),
        'Please specify the language of the book (de, en or se)'
    );

    if (!isNil(bookTypes)) {
        bookTypes.forEach((type: EbookType) =>
            console.assert(
                VALID_EBOOK_TYPES.includes(type),
                `'${type}' is not a valid book type.`
            )
        );
    }

    const directory = directoryOfLanguage(projectRoot, language);
    await createBooks(directory, config, language, bookTypes, {
        build,
        commit
    });
};

// const normalize = async (projectRoot: string, language: string): Promise<void> => {
// 	console.assert(
// 		language !== '' && ['de', 'en', 'se'].includes(language),
// 		'Please specify the language of the book (de, en or se)',
// 	);

// 	const directory = directoryOfLanguage(projectRoot, language);
// 	await normalizeBooks(directory);
// };

(async () => {
    const projectRoot = getProjectRoot();

    program
        .name('ewer')
        .description(
            'Ewer transcribes audio files, translates for books, websites and apps, and builds ebooks and books.'
        )
        .version(version);

    program
        .command('transcribe')
        .description(
            'Transcribes mp3 files into markdown files with attribution.'
        )
        .option('-l, --language [en | de | se]', 'Specify the language', 'en')
        .option(
            '-a, --audioPath [/path/to/audio.mp3]',
            'Path to audio file (MP3)',
            '.'
        )
        .option(
            '-x, --textPath [/path/to/transscribed.md]',
            'Path to output text file (MD)',
            '.'
        )
        .option('-C, --contentPath [/path/to/content]', 'Path to app content')
        .option(
            '-S, --storagePath [/path/to/storage]',
            'Path to storage content'
        )
        .action(async ({ language, audioPath, textPath }) => {
            await transcribeAudioBook(language, audioPath, textPath);
        });

    program
        .command('build')
        .description('Build ebooks, printable books, rtf and text files.')
        .option('-l, --language [en | de | se]', 'Specify the language', 'en')
        .option(
            '-t, --types [epub,pdf,txt,rtf]',
            'Specify the type of book, default: epub',
            'epub'
        )
        .option('-b, --build [build number]', 'Pipeline build number')
        .option('-c, --commit [git commit]', 'Commit Id')
        .action(async ({ language, types, build: buildnr, commit }) => {
            const { book: bookConfig } = await getProductionOptions();
            const bookTypes = types.split(',') as EbookType[];

            if (isNil(bookConfig)) {
                throw new Error(
                    'I do not find a book config in ewer.config.js'
                );
            }

            await build(
                projectRoot,
                bookConfig,
                language,
                bookTypes,
                buildnr,
                commit
            );
        });

    program
        .command('translate')
        .description('Translate text files and ebooks.')
        .option(
            '-l, --supportedLanguages',
            'List supported languages for translation.'
        )
        .option(
            '-s, --sourceLanguage [deepl language code]',
            'Specify the source language',
            defaultSourceLanguage
        )
        .option(
            '-t, --targetLanguage [deepl language code]',
            'Specify the target language',
            defaultTargetLanguage
        )
        .option(
            '-f, --sourceFile [path to .md file or directory]',
            'Specify file or directory to translate',
            '.'
        )
        .option(
            '-d, --destFile [path to .md file or directory]',
            'Specify file or directory to store the translation'
        )
        .option(
            '-d, --languageDirectories [string array with directories to translate]',
            'Use language code directories for autotranslating',
            '.'
        )
        .action(
            async ({
                supportedLanguages,
                sourceLanguage,
                targetLanguage,
                sourceFile,
                destFile
            }) => {
                if (supportedLanguages === true) {
                    const languages = await listLanguages();
                    console.info('Target Languages');
                    console.table(
                        languages.target.sort((a, b) =>
                            a.name < b.name ? -1 : 1
                        )
                    );
                } else {
                    await translate({
                        sourceLanguage,
                        targetLanguage,
                        sourceFile,
                        destFile
                    });
                }
            }
        );

    await program.parseAsync(process.argv);
})().catch((err) => {
    const error = err as Error;

    console.error(error);
    process.exit(1);
});
