import { isNil } from 'lodash';
import { google } from '@google-cloud/speech/build/protos/protos.d';
import { SpeechClient } from '@google-cloud/speech/build/src/v1p1beta1';
import { FileDescriptor } from '../../types';
import {
    DEFAULT_SAMPLE_RATE_HERTZ,
    googleCredentialsWarning
} from '../../common';

interface IStream {
    name: string;
    timeElapsed: number;
}

interface ITranscriptionResult {
    transcription: string;
    languageCode?: string;
}

interface ITimeObject {
    startTime: {
        seconds: string;
        nanos: number;
    };
    lastUpdateTime: {
        seconds: string;
        nanos: number;
    };
    uri: string;
}

export class GoogleCloudSpeechApi {
    static streams: IStream[] = [];
    language: string | undefined = '';

    constructor(language: string | undefined) {
        if (isNil(process.env.GOOGLE_APPLICATION_CREDENTIALS)) {
            throw new Error(googleCredentialsWarning);
        }

        this.language = language;
    }

    async convertSpeechToText(
        audioFile: FileDescriptor
    ): Promise<ITranscriptionResult> {
        if (isNil(audioFile)) {
            return { transcription: '' };
        }

        const client = new SpeechClient();

        const encoding = 'MP3';
        const config: google.cloud.speech.v1p1beta1.IRecognitionConfig = {
            encoding,
            sampleRateHertz: DEFAULT_SAMPLE_RATE_HERTZ,
            languageCode: audioFile.languageCode
        };

        const audio = {
            uri: audioFile.url
        };

        const request: google.cloud.speech.v1p1beta1.ILongRunningRecognizeRequest =
            {
                config,
                audio
            };

        console.log(
            `Transcribing '${audioFile.name}' with language code '${
                audioFile.languageCode ?? 'unknown'
            }' ...`
        );

        const [operation] = await client.longRunningRecognize(request);
        operation.on('progress', (event: ITimeObject): void =>
            GoogleCloudSpeechApi.logStatus(event, audioFile.name)
        );
        const [response] = await operation.promise();

        GoogleCloudSpeechApi.clearStatus(audioFile.name);

        if (!isNil(response)) {
            const transcription = response.results
                ?.map(
                    (
                        result: google.cloud.speech.v1p1beta1.ISpeechRecognitionResult
                    ) => {
                        return result.alternatives
                            ?.map((alt) => alt.transcript)
                            .join('\n(Alternative:)');
                    }
                )
                .join('\n');

            return { transcription: transcription ?? '' };
        }

        return { transcription: '' };
    }

    static logStatus(event: ITimeObject, name: string): void {
        const logEntry = GoogleCloudSpeechApi.streams.find(
            (stream) => stream.name === name
        );
        const timeElapsed: number =
            parseInt(event.lastUpdateTime.seconds) -
            parseInt(event.startTime.seconds);

        if (isNil(logEntry)) {
            GoogleCloudSpeechApi.streams.push({
                name,
                timeElapsed
            });
        } else {
            logEntry.timeElapsed = timeElapsed;
        }

        GoogleCloudSpeechApi.printStatus();
    }

    static clearStatus(name: string): void {
        const index = GoogleCloudSpeechApi.streams.findIndex(
            (stream) => stream.name === name
        );
        if (index !== -1) {
            GoogleCloudSpeechApi.streams.splice(index, 1);
        }

        GoogleCloudSpeechApi.printStatus();
    }

    static printStatus(): void {
        console.log(
            GoogleCloudSpeechApi.streams
                .map((stream) => `${stream.name}: ${stream.timeElapsed} sec.`)
                .join('\n')
        );
    }
}

export default GoogleCloudSpeechApi;
