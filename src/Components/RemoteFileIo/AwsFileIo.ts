import AWS from 'aws-sdk';
import { isNil } from 'lodash';

type IAccessType = 'public-read';

interface IFileIoOptions {
    bucket: string;
    accessType?: IAccessType;
}

const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
let awsRegion = process.env.AWS_REGION;
awsRegion = !isNil(awsRegion) ? awsRegion : '';

export class AwsFileIo {
    S3: AWS.S3 | null = null;
    bucket = '';
    accessType: IAccessType = 'public-read';

    constructor(options: IFileIoOptions) {
        if (isNil(accessKeyId)) {
            throw new Error('Env variable AWS_ACCESS_KEY_ID missing.');
        }
        if (isNil(secretAccessKey)) {
            throw new Error('Env variable AWS_SECRET_ACCESS_KEY missing.');
        }

        this.S3 = new AWS.S3({
            apiVersion: '2006-03-01',
            credentials: {
                accessKeyId,
                secretAccessKey
            }
        });

        this.bucket = options.bucket;
        this.accessType =
            options.accessType !== undefined
                ? options.accessType
                : 'public-read';
    }

    async upload(
        filePath: string,
        fileContent: string | Buffer
    ): Promise<string> {
        return await new Promise((resolve, reject) => {
            if (isNil(this.S3)) {
                throw new Error('I do not have a S3 environment.');
            }

            this.S3.upload(
                {
                    ACL: this.accessType,
                    Body: fileContent,
                    Bucket: this.bucket,
                    Key: filePath
                },
                (err: Error) => {
                    if (!isNil(err)) {
                        reject(err);
                    } else {
                        const href = `https://${this.bucket}.s3.${
                            awsRegion ?? ''
                        }.amazonaws.com/${filePath.replace(/\s/g, '+')}`;
                        resolve(href);
                    }
                }
            );
        });
    }
}

export default AwsFileIo;
