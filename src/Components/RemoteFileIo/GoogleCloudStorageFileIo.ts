import path from 'path';

import { isNil } from 'lodash';
import { Storage, UploadOptions } from '@google-cloud/storage';
import { googleCredentialsWarning } from '../../common';

interface IFileIoOptions {
    bucketName: string;
}

interface IUploadReturn {
    publicUrl: string;
    internalUrl: string;
}

export class GoogleCloudStorageFileIo {
    storage: Storage | null = null;
    bucketName = '';

    constructor(options: IFileIoOptions) {
        if (isNil(process.env.GOOGLE_APPLICATION_CREDENTIALS)) {
            throw new Error(googleCredentialsWarning);
        }

        this.storage = new Storage();
        this.bucketName = options.bucketName;
    }

    publicPath(): string {
        return `https://storage.googleapis.com/${this.bucketName}`;
    }

    async upload(
        filePath: string,
        options: UploadOptions = {}
    ): Promise<IUploadReturn | null> {
        if (isNil(this.storage)) {
            console.error('I do not have a storage environment.');
            return null;
        }
        const bucket = this.storage.bucket(this.bucketName);
        const fileName = path.basename(filePath);
        const file = bucket.file(fileName);

        try {
            await bucket.upload(filePath, options);

            await file.makePublic();

            return {
                internalUrl: `gs://${this.bucketName}/${fileName}`,
                publicUrl: `${this.publicPath()}/${fileName}`
            };
        } catch (err) {
            const error = err as Error;
            console.error(error.message);
            return null;
        }
    }
}

export default GoogleCloudStorageFileIo;
