import YAML from 'yaml';
import { isNil, get } from 'lodash';
import getProductionOptions from '../../common/getProductionOptions';
import {
    EwerConfig,
    FrontMatterDefaults,
    CustomFrontMatterProp
} from '../../types';

type PredefiendFrontMatterVariable =
    | 'aliases'
    | 'audio'
    | 'cascade'
    | 'date'
    | 'description'
    | 'draft'
    | 'expiryDate'
    | 'headless'
    | 'images'
    | 'image'
    | 'isCJKLanguage'
    | 'keywords'
    | 'layout'
    | 'lastmod'
    | 'linkTitle'
    | 'markup'
    | 'outputs'
    | 'publishDate'
    | 'resources'
    | 'series'
    | 'slug'
    | 'summary'
    | 'title'
    | 'type'
    | 'url'
    | 'videos'
    | 'weight';

type FrontMatterVariable = PredefiendFrontMatterVariable | string;
type FrontMatterVar = string | number | boolean | string[];
type FrontMatterVars = {
    [Property in FrontMatterVariable]?: FrontMatterVar;
};

const translatableAttributes = [
    'description',
    'topic',
    'keywords',
    'linkTitle',
    'summary',
    'title'
];

class HugoFile {
    public frontMatterVars: FrontMatterVars = {};
    public frontMatterTranslatables: string[] = [];
    public translatableText = '';
    public translatableFrontMatter: string[] = [];
    public textKeys: string[] = [];

    private frontMatterText = '';

    constructor(text: string, options: EwerConfig) {
        const customFrontMatterProps = get(
            options,
            'translation.customFrontMatterProps',
            []
        ) as CustomFrontMatterProp[];
        const frontMatterDefaults = get(
            options,
            'translation.frontMatterDefaults',
            {}
        ) as FrontMatterDefaults;

        if (!isNil(customFrontMatterProps)) {
            translatableAttributes.push(
                ...customFrontMatterProps
                    .filter((prop) => prop.translatable === true)
                    .map((prop) => prop.prop)
            );
        }

        this.readFrontMatter(text, frontMatterDefaults);
    }

    get sourceText() {
        const additionalLines = isNil(this.frontMatterVars.translation)
            ? ['translation: original']
            : [];

        return `---\n${this.frontMatterText}\n${additionalLines.join(
            '\n'
        )}\n---\n${this.translatableText}`;
    }

    readFrontMatter(text: string, frontMatterDefaults: FrontMatterDefaults) {
        const frontMatterRegex = /^---\r?\n([\s\S]*?)\r?\n---\r?\n([\s\S]*)$/;
        const matches = frontMatterRegex.exec(text);

        if (matches === null) {
            this.translatableText = text;
            this.translatableFrontMatter = [];
            this.textKeys = [];
            this.frontMatterVars = {
                title: 'A title',
                date: new Date().toString(),
                description: 'A description',
                type: 'A type',
                image: 'A relative image URL',
                author: 'An author',
                categories: ['1', '2', '3'],
                tags: ['A', 'B', 'C'],
                translation: 'original',
                ...frontMatterDefaults
            };

            this.frontMatterText = YAML.stringify(this.frontMatterVars);

            return;
        }
        try {
            this.frontMatterVars = YAML.parse(matches[1]);
            this.frontMatterText = matches[1];
            this.translatableText = matches[2];
        } catch (err) {
            const error = err as Error;
            console.error(
                `Front matter vars found, but not valid YAML format.\n${error.name}: ${error.message}\n\n${matches[1]}.`
            );
            process.exit(1);
        }

        if (this.frontMatterVars === null) {
            return;
        }

        const [translatables, keys] = Object.keys(this.frontMatterVars).reduce<
            [string[], string[]]
        >(
            (acc: [string[], string[]], prop: FrontMatterVar) => {
                if (
                    isNil(this.frontMatterVars) ||
                    !translatableAttributes.includes(prop as string)
                ) {
                    return acc;
                }

                const value = this.frontMatterVars[prop as FrontMatterVariable];

                if (Array.isArray(value)) {
                    return [
                        [...acc[0], ...value],
                        [
                            ...acc[1],
                            ...new Array((value as string[]).length).fill(prop)
                        ]
                    ];
                } else {
                    return [
                        [...acc[0], value as string],
                        [...acc[1], prop]
                    ];
                }
            },
            [[], []] as [string[], string[]]
        );

        this.translatableFrontMatter.push(...translatables);
        this.textKeys.push(...keys);
    }

    getDestText(mainText: string, frontMatter: string[]) {
        if (frontMatter.length !== this.textKeys.length) {
            throw new Error(
                `I got ${frontMatter.length} translated strings, but have ${this.textKeys.length} string keys. Aborting ...`
            );
        }

        const translatedFrontMatter: FrontMatterVars = {};
        this.textKeys.forEach((key, i) => {
            const value = frontMatter[i];
            const type = typeof this.frontMatterVars[key];

            if (type === 'string' || type === 'number' || type === 'boolean') {
                translatedFrontMatter[key] = value;
            } else {
                const frontMatter = translatedFrontMatter[key] as string[];
                if (isNil(frontMatter)) {
                    translatedFrontMatter[key] = [value];
                } else {
                    frontMatter.push(value);
                }
            }
        });

        const frontMatterVars = {
            ...this.frontMatterVars,
            ...translatedFrontMatter,
            translation: 'auto'
        };

        const frontMatterString = YAML.stringify(frontMatterVars);
        return `---\n${frontMatterString}\n---\n${mainText}`;
    }
}

export default HugoFile;
