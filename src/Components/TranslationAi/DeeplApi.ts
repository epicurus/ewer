import * as deepl from 'deepl-node';
import { isNil } from 'lodash';
import { SupportedLanguages } from '../../types';
import { deeplCredentialsWarning } from '../../common/warningsAndErrors';

const apiKey = process.env.DEEPL_API_KEY;
const baseUrl = process.env.DEEPL_BASE_URL;

if (isNil(apiKey) || isNil(baseUrl)) {
    throw new Error(deeplCredentialsWarning);
}

export class DeeplApi {
    private translator: deepl.Translator;
    private sourceLanguages: readonly deepl.Language[] | null = null;
    private targetLanguages: readonly deepl.Language[] | null = null;

    constructor() {
        this.translator = new deepl.Translator(apiKey as string);
    }

    async init(): Promise<void> {
        if (this.sourceLanguages !== null && this.targetLanguages !== null) {
            return;
        }

        const [sourceLanguages, targetLanguages] = await Promise.all([
            this.translator.getSourceLanguages(),
            this.translator.getTargetLanguages()
        ]);

        this.sourceLanguages = sourceLanguages;
        this.targetLanguages = targetLanguages;
    }

    listLanguages(): SupportedLanguages {
        if (this.sourceLanguages === null || this.targetLanguages === null) {
            throw new Error('I could not load the supported languages.');
        }

        return {
            source: this.sourceLanguages as deepl.Language[],
            target: this.targetLanguages as deepl.Language[]
        };
    }

    isValidTargetLanguage(languageCode: string): boolean {
        if (this.targetLanguages === null) {
            throw new Error('I could not load the supported languages.');
        }

        if (
            this.targetLanguages.find(
                (language) => language.code === languageCode
            )
        ) {
            return true;
        }

        return false;
    }

    isValidSourceLanguage(languageCode: string): boolean {
        if (this.sourceLanguages === null) {
            throw new Error('I could not load the supported languages.');
        }

        if (
            this.sourceLanguages.find(
                (language) => language.code === languageCode
            )
        ) {
            return true;
        }

        return false;
    }

    async translateMarkdown(
        texts: string[],
        targetLang: deepl.TargetLanguageCode,
        sourceLang: deepl.SourceLanguageCode | null = null
    ): Promise<deepl.TextResult[]> {
        const textToTranslate = texts.filter(
            (text) => text !== '' && !isNil(text)
        );
        return textToTranslate.length > 0
            ? this.translator.translateText(texts, sourceLang, targetLang)
            : [];
    }
}

export default new DeeplApi();
