<div class="impressum">
    <strong>Bitbucket</strong>:
    <!-- if booktype is epub --><a href="${BOOK_REPOSITORY}">... repository name ...</a><br /><!-- endif booktype -->
    <!-- if booktype is pdf --><span>${BOOK_REPOSITORY}</span><!-- endif booktype -->
    <br />
    <!-- if booktype is epub --><strong>ASIN</strong>: ${BOOK_ASIN}<br /><!-- endif booktype -->
    <!-- if booktype is pdf --><strong>ISBN</strong>: ${BOOK_ISBN}<br /><!-- endif booktype -->
    <strong>Version</strong>: ${BOOK_VERSION}.${BITBUCKET_BUILD_NUMBER}.${BITBUCKET_COMMIT}<br />
    <strong>License</strong>: ${BOOK_LICENSE}<br />
    <strong>Contributors</strong>:<br />
    ... contributor 1 ...<br />
    ... contributor 2 ...<br />
    <strong>Uploads</strong>:<br />
    ${RAW_BOOK_URL}
</div>

<div class="title-page">${BOOK_TITLE}</div>
<div style="visibility: hidden;">\pagebreak</div>

