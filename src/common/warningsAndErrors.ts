import chalk from 'chalk';

export const googleCredentialsWarning = `
${chalk.red(`I'm missing the Google credentials file. You can get one at 'https://console.cloud.google.com'.
It looks like:`)}
${chalk.redBright(`
    {
        "type": "service_account",
        "project_id": "ebook-maker-111222333444555",
        "private_key_id": "122134234da324234234adaaddaddaad1",
        "private_key": "-----BEGIN PRIVATE KEY-----MII....wRA88Jtu5VH-----END PRIVATE KEY-----",
        "client_email": "starting-account-at45453ssaadd-@ebook-maker-3232424422424.iam.gserviceaccount.com",
        "client_id": "1111112222233333444444",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/..."
    }
`)}
${chalk.green(`And please enable following APIs:

    Cloud Speech-to-Text API
    Cloud Storage

And store the location of the credentials file in the GOOGLE_APPLICATION_CREDENTIALS env variable.

To enable Cloud storage you have to navigate to

    https://console.cloud.google.com/storage/browser

create a bucket

    ewer

and under permissions of this bucket add the role

    Storage-Administrator

to the user you find inthe 'client_email' property of the Google credentials file.
`)}`;

export const deeplCredentialsWarning = `
${chalk.red(`I'm missing the DEEPL Keys. It looks like

DEEPL_API_KEY=DeepL-Auth-Key xxxxxxxx-xxxx-...
DEEPL_BASE_URL=https://api-free.deepl.com/v2
`)}

${chalk.green(`
You can get the api key by logging into deepl.com and checking the examples: https://www.deepl.com/docs-api.
If you have a API or PRO account you find your key there.
`)}`;
