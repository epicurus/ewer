import fs from 'fs';
import { isNil } from 'lodash';

export const readChapters = async (directory: string): Promise<string[]> => {
    return await new Promise((resolve, reject) => {
        fs.readdir(directory, (err, files) => {
            if (!isNil(err)) {
                reject(err);
                return;
            }

            files = files
                .filter((file) => {
                    return !isNil(file.match(/^\d{2,4}/));
                })
                .sort();

            resolve(files);
        });
    });
};
