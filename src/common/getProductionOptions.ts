import { merge, isNil } from 'lodash';
import getProjectRoot from './getProjectRoot';
import type { EwerConfig } from '../types';

const defaultConfig: EwerConfig = {
    book: {
        meta: {
            version: '1.0.0',
            type: 'ewer:ebook'
        },
        general: {
            bookTypesToUpload: ['rtf']
        },
        metadata: {
            title: 'Ewer E-Book',
            authors: 'No author',
            outputProfile: 'kindle',
            bookProducer: 'ewer',
            comments: 'NO LICENSE',
            isbn: '',
            pubdate: '',
            publisher: ''
        },
        additionalMetadata: {
            asin: '',
            version: '1.0.0',
            license: 'NO LICENSE',
            repository: '',
            rawBookUrl:
                'Place a url here or let it be fill automatically with uploaded books',
            stripHeadlinesAndFormatsFrom: ['rtf', 'txt']
        },
        lookAndFeel: {
            extraCss: '',
            embedAllFonts: true,
            lineHeight: 22,
            changeJustification: 'justified'
        },
        txtInputOptions: {
            formattingType: 'markdown'
        },
        mobiOutputOptions: {
            mobiFileType: 'both'
        },
        epubOutputOptions: {
            epubInlineToc: false,
            epubTocAtEnd: false
        },
        azw3OutputOptions: {
            prettyPrint: true
        },
        pdfOutputOptions: {
            customSizeCm: '12x19',
            customSize: '',
            paperSize: 'a5',
            pdfPageNumbers: true,
            prettyPrint: true,
            pdfDefaultFontSize: 14.5,
            pdfAddToc: true,
            pdfHyphenate: true,
            pdfOddEvenOffset: 0,
            pdfPageMarginTop: 36.0,
            pdfPageMarginBottom: 36.0,
            pdfPageMarginLeft: 36.0,
            pdfPageMarginRight: 36.0
        },
        txtOutputOptions: {
            inlineToc: false,
            keepColor: false,
            keepImageReferences: false,
            keepLinks: false,
            prettyPrint: true,
            txtOutputFormatting: 'plain'
        },
        rtfOutputOptions: {
            prettyPrint: true
        },
        tableOfContents: {
            tocTitle: 'Content',
            level1Toc: '//h:h2',
            useAutoToc: true
        }
    },
    translation: {
        customFrontMatterProps: [],
        fileExtensions: ['md', 'markdown'],
        defaultLanguageCodes: {
            en: 'en-GB',
            pt: 'pt-PT'
        }
    }
};

const projectRoot = getProjectRoot();

export default async (): Promise<EwerConfig> => {
    const configStub = await import(`${projectRoot}/ewer.config`);

    if (isNil(configStub)) {
        console.warn(
            `I didn't find a ewer.config.js in directory '${projectRoot}', and need it for building ebooks. Taking the dafault ...`
        );

        return defaultConfig;
    }
    return merge(defaultConfig, configStub);
};
