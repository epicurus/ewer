import { isNil } from 'lodash';
import { CustomSize } from '../types';

const roundString = (number: string, translate = 1): number => {
    return Math.round(parseFloat(number) * translate * 1000) / 1000;
};

export const getWidthAndHeightOfCustomSize = (
    customSize: string,
    translate = 1
): CustomSize => {
    const re = /([\d.]+)x([\d.]+)/;
    const match = customSize.match(re);
    let width = 0;
    let height = 0;
    if (isNil(match)) {
        console.error(
            `ERROR: '${customSize}' has not a valid format (dd.dxdd.d)`
        );
    } else {
        width = roundString(match[1], translate);
        height = roundString(match[2], translate);
    }

    return { width, height };
};
