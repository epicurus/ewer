import { isNil } from 'lodash';
import { google } from '@google-cloud/speech/build/protos/protos.d';
import { SpeechClient } from '@google-cloud/speech/build/src/v1p1beta1';
import { FileDescriptor } from '../types';
import { LANGUAGE_CODES, DEFAULT_SAMPLE_RATE_HERTZ } from '../common';

export const convertSpeechToText = async (
    audioFile: FileDescriptor,
    language: string
): Promise<string | null> => {
    if (isNil(audioFile)) {
        return null;
    }
    // Creates a client
    const client = new SpeechClient();

    const encoding = 'MP3';
    const languageCode = LANGUAGE_CODES[language];

    const config: google.cloud.speech.v1p1beta1.IRecognitionConfig = {
        encoding,
        sampleRateHertz: DEFAULT_SAMPLE_RATE_HERTZ,
        languageCode
        // sampleRateHertz: 44100,
        // languageCode: 'de-DE',
    };

    const audio = {
        uri: audioFile.url
    };

    const request: google.cloud.speech.v1p1beta1.ILongRunningRecognizeRequest =
        {
            config,
            audio
        };
    const [operation] = await client.longRunningRecognize(request);
    operation.on('progress', (event) => console.log(JSON.stringify(event)));
    const [response] = await operation.promise();

    if (!isNil(response) && !isNil(response.results)) {
        const transcription = response.results
            .map(
                (
                    result: google.cloud.speech.v1p1beta1.ISpeechRecognitionResult
                ) =>
                    !isNil(result.alternatives)
                        ? result.alternatives[0].transcript
                        : ''
            )
            .join('\n');

        return transcription;
    }

    return '';
};
