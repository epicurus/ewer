import { Language } from 'deepl-node';
import { VALID_EBOOK_TYPES } from './common';

export type EbookType = (typeof VALID_EBOOK_TYPES)[number];

export interface GeneralBookProductionOptions {
    bookTypesToUpload: EbookType[];
}

export interface CustomSize {
    width: number;
    height: number;
}

interface KindleMetadata {
    outputProfile: 'kindle';
    authors: string;
    bookProducer: string;
    comments: string;
    isbn: string;
    pubdate: string;
    publisher: string;
    title: string;
}

interface AdditionalMetadata {
    asin: string;
    version: string;
    license: string;
    repository?: string;
    commit?: string;
    build?: string;
    rawBookUrl?: string;
    stripHeadlinesAndFormatsFrom: EbookType[];
}

interface LookAndFeel {
    extraCss: string;
    embedAllFonts: boolean;
    lineHeight: number;
    changeJustification: 'original' | 'left' | 'justified';
}

interface TxtInputOptions {
    formattingType: 'markdown';
}

interface MobiOutputOptions {
    mobiFileType: 'old' | 'both' | 'new';
}

interface EpubOutputOptions {
    epubInlineToc: boolean;
    epubTocAtEnd: boolean;
}

interface Azw3OutputOptions {
    prettyPrint: boolean;
}

type IPaperSize =
    | 'a0'
    | 'a1'
    | 'a2'
    | 'a3'
    | 'a4'
    | 'a5'
    | 'a6'
    | 'b0'
    | 'b1'
    | 'b2'
    | 'b3'
    | 'b4'
    | 'b5'
    | 'b6'
    | 'legal'
    | 'letter';

export interface PdfOutputOptions {
    customSize: string;
    customSizeCm?: string;
    paperSize: IPaperSize;
    pdfPageNumbers: boolean;
    prettyPrint: boolean;
    pdfDefaultFontSize: number;
    pdfAddToc: boolean;
    pdfHyphenate: boolean;
    pdfOddEvenOffset: number;
    pdfPageMarginTop: number;
    pdfPageMarginBottom: number;
    pdfPageMarginLeft: number;
    pdfPageMarginRight: number;
}

type TxtOutputFormatting = 'plain' | 'markdown' | 'textile';

interface TxtOutputOptions {
    forceMaxLineLength?: number;
    inlineToc: boolean;
    keepColor: boolean;
    keepImageReferences: boolean;
    keepLinks: boolean;
    prettyPrint: boolean;
    txtOutputFormatting: TxtOutputFormatting;
}

interface RtfOutputOptions {
    prettyPrint: boolean;
}

interface TableOfContents {
    tocTitle: string;
    level1Toc: '//h:h1' | '//h:h2' | '//h:h3';
    level2Toc?: '//h:h2' | '//h:h3' | '//h:h4';
    level3Toc?: '//h:h3' | '//h:h4' | '//h:h5';
    useAutoToc: boolean;
}

export type MarkdownVariables = Record<string, string>;

export interface BookProductionOptions {
    general: GeneralBookProductionOptions;
    metadata: KindleMetadata;
    additionalMetadata: AdditionalMetadata;
    lookAndFeel: LookAndFeel;
    txtInputOptions: TxtInputOptions;
    mobiOutputOptions: MobiOutputOptions;
    epubOutputOptions: EpubOutputOptions;
    azw3OutputOptions: Azw3OutputOptions;
    pdfOutputOptions: PdfOutputOptions;
    txtOutputOptions: TxtOutputOptions;
    rtfOutputOptions: RtfOutputOptions;
    tableOfContents: TableOfContents;
    meta: {
        version: string;
        type: string;
    };
}

export interface CustomFrontMatterProp {
    prop: string;
    translatable: boolean;
}
export interface FrontMatterDefaults {
    title?: string;
    description?: string;
    type?: string;
    author?: string;
}
export interface TranslationOptions {
    customFrontMatterProps?: CustomFrontMatterProp[];
    fileExtensions?: string[];
    defaultLanguageCodes?: { [key: string]: string };
    frontMatterDefaults?: FrontMatterDefaults;
}

export interface EwerConfig {
    book?: BookProductionOptions;
    translation?: TranslationOptions;
}

export type EbookConvertOptions = Record<string, string | number | boolean>;

export type Metadata = KindleMetadata & AdditionalMetadata;
export interface AdditionalParameter {
    build: string;
    commit: string;
}
export type FileDescriptor = {
    epikuretId?: string;
    name: string;
    ext: string;
    url?: string;
    title?: string;
    author?: string;
    publicAudioUrl?: string;
    videoExt?: string;
    publicVideoUrl?: string;
    languageCode?: string;
    shouldTranscribe?: boolean;
    shouldUpload?: boolean;
    shouldCreateMarkdown?: boolean;
} | null;

export type EpikuretType =
    | 'epicurus'
    | 'thought'
    | 'book'
    | 'audio'
    | 'glossar'
    | 'video';

export type BuyProp = Record<string, Array<Record<string, string>>>;

export interface EpikuretEntry {
    epikuretId: string;
    title: string;
    subtitle?: string;
    origin?: string;
    author?: string;
    year?: string;
    type: EpikuretType;
    audio?: string;
    language?: string;
    published?: boolean;
    promotion?: string;
    dialog?: string[];
    thought?: string[];
    book?: string;
    audiobook?: string;
    ebook?: string;
    cover?: string;
    download?: string;
    buy?: BuyProp;
}

type SubtitleSnippet = Record<string, Record<string, string>>;

export interface Config {
    subtitleSnippets?: SubtitleSnippet;
}

export interface SupportedLanguages {
    source: Language[];
    target: Language[];
}
